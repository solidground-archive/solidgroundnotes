---
layout: default
title: Design your solution
nav_order: 1
permalink: floorplanner/designer
---

# Design with Floorplanner
{: .fs-9 .no_toc }

Follow best-practice procedures and gradually shape your solution. Floorplanner will guide you along.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> This documentation section will offer guidance for working with Floorplanner when the project matures beyond initial stages.

## Overview

Floorplanner forms the heart of the Solidground project. Here is where [Process Recipes](/process/recipes) will be adopted and presented in the online design space. Initially that will be as static documentation, but then increasingly elements of those will be supported by automation to help Creators.

The design of a social experience involves following a Process Playbook - a set of recipes that is put together by the creators to match their preferred workflow. The diagram below shows how Floorplanner fits in the Solidground project:

![Diagram of Floorplanner, as described in the text](/assets/images/solidground-product-overview.png)

While the creator walks through the step-by-step guidance of the process playbook, state about the social experience is tracked. This state consists of:

- **Living specs**: Up-to-date machine- and human-readable specs that are in-sync with the codebase.
- **Living docs**: Update documentation that reflects the current state of the solution design.
- **Codebase**: Scaffolded code generated from the specs, and state about the development project.
- **Configuration**: Configuration files needed during the experience design and for deployment.

As Process Recipes are followed gradually the Blueprint of the social experience is further elaborated. As soon as sufficient project artifacts are available it is possible to 'eject' the social experience and host it on the Groundwork service platform for testing or deployment to production.

### Focus areas

| Key focus area | Description |
| :--- | :--- |
| [**Practice social coding**](/process/focus/social-coding) | Floorplanner follows an inclusive approach to software development that involves everyone in the creation process, recognizes their skills, and provides optimal conditions for collaboration. |
| [**Provide process guidance**](/process/focus/process-guidance) | Floorplanner offers a growing list of Process Recipes to guide creators through important tasks. Recipes contain best-practices, background information, step-by-step procedures and checklists. |
| [**Deliver integrated experiences**](/process/focus/integrated-experience) | Floorplanner's primary goal is for everyone involved in the solution design to _'think beyond the app'_. Create focus on common understanding and finding people's true needs. Social aspects drive the process. |
| [**Elicit joyful creation**](process/focus/social-coding/joyful-creation) | Floorplanner aims to take away the chores and improve collaboration. Process Recipes are improved and automated over time, so that creators can focus more on the fun parts: to deliver the features that clients need. |

For more information about these focus areas, please visit [Taskweave documentation](/process/focus).

### Target audience

For Floorplanner we discern two different high-level _stakeholder groups_ who are in close interaction throughout the design process. People in these groups have multiple roles, which will be considered where that makes sense.

<table>
  <thead>
    <tr>
      <th width="40%">Stakeholder Group</th>
      <th width="60%">Key benefits</th>
    </tr>
  </thead>
  <tbody style="vertical-align: top;">
    <tr>
      <td><b>Creators</b>: Primary stakeholder <p>Anyone involved with design and implementation of a social experience in a Free Software project. Covers all software development roles.</p></td>
      <td>
        <ul>
          <li>Creators include everyone involved in Free Software development projects that go beyond hobbyism.</li>
          <li>Creators should focus primarily on tasks that are relevant to the business domain of the social experience they design.</li>
          <li>Creators need guidance and best-practices and the tools and processes that allow to adhere to them properly.</li>
          <li>Creators want to be shielded to complexity of integrating experiences into larger decentralized social networks.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Clients</b>: Secondary stakeholder <p>The people who need the social experience. They are the 'customer' and the domain experts whose knowledge is input to experience design.</p></td>
      <td>
        <ul>
          <li>Clients want to see their needs covered in the social experience as the solution matures, and be able to steer solution design along the way.</li>
          <li>Clients want solutions able to evolve as needs change over time, and continue to extend or change their capabilities.</li>
          <li>Clients want quality, short dev-to-prod cycles, and integration of solutions with other social experiences that exist.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<br/>


