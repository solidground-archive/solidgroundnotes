---
layout: default
title: Getting started
nav_order: 1
description: "Social Experience Design for the Fediverse"
permalink: /
---

# Welcome to Solidground
{: .fs-9 .no_toc }

Social Experience Design for the Fediverse
{: .fs-6 .fw-500 }

[Join Our Chat](https://matrix.to/#/#solidground-matters:matrix.org){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Visit Our Forum](https://discuss.coding.social/c/solidground/13){: .btn .fs-5 .mb-4 .mb-md-0 } [Go To The Code](https://codeberg.org/solidground/){: .btn .fs-5 .mb-4 .mb-md-0 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> **Solidground is still in an early stage of development.** Using [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html) this site will update frequently.

## Introduction

Solidground aims to significantly _ease development_ of new applications and services for the Fediverse, that can be deeply integrated with one another. Instead of apps we call these **social experiences**.

{: .aside}
> Fediverse faces [major social challenges](https://discuss.coding.social/t/major-challenges-for-the-fediverse/67) that hamper its healthy evolution. Solidground brings socio-technical support and tools, to help the Fediverse reach its full potential. Solidground offers **Social experience design** (SX) for Creators and their Clients.

Solidground project provides three services to help you go along an **experience design** from start to finish.

| Focus area | The help we provide |
| :--- | :--- |
| [Improve your process](/process/recipes) | **Taskweave** is a crowdsourced pattern library of process recipes containing best-practices, step-by-step guides, templates and checklists to make creation of your social experience more productive and fun. Use this guidance to improve your solution designs. |
| [Design your blueprint](/floorplanner/design) | **Floorplanner** is a design tool that allows you to focus on your domain to elicit the right features to build. It automates many tasks, does code generation, and makes your specifications and docs an integral part of the social experience. |
| [Host your experience](/_groundwork/platform) | **Groundwork** facilitates hosting of social experiences on the Fediverse and makes administration and maintenance easy. Launching a solution from floorplanner involves ejecting a full Groundwork system, or deploy pluggable service modules to an already running system. |

## Enter Social Experience Design

Solidground project focuses on Social Experience Design, which is abbreviated as SX. Our main offering, the [Floorplanner](/floorplanner/design), offers best-practices, tools and automation that emphasize and ease the _Process of Creating_ social experiences for the Fediverse. This allows you to:

- **Truly address people's needs**: Provide a natural transition from requirements to code and features, whereby timely feedback assures your project remains on track.
- **Ensure active participation**: Stimulate inclusion and collaboration so that everyone involved in your project is on the same page and remains actively engaged throughout the project lifecycle.
- **Emphasize the Joy of Creation**: Coding, design and hosting of a social experience should be fun. We strive to automate many of your development chores, and provide guidance wherever we can.
- **Delivery of evolvable solutions**: Ability to grow your app from its early beginnings to increase its value and use, and streamline integration with the social fabric the Fediverse provides.

Solidground helps guide the development process, work on healthy Free Software projects, and bring solutions that delight. [➥ Learn more and explore SX ..](/process/focus)

## The Floorplanner Experience

Floorplanner is an online designer that assists in the creation of social experiences. Based on Process Recipes defined in Taskweave, creators and clients are guided through the development process and design a Solution Blueprint of their social experience. Once the solution is ready to be hosted on Groundwork, it is ejected to a production instance.

### Floorplanner development project

Floorplanner experience design focuses on the entire Free Software Development Lifecycle (FSDL). In fact _Floorplanner itself is a social experience_. This means that for the product development of Floorplanner we can "eat our own dog food" along the way. This gives interesting possibilities:

- Floorplanner is federated, self-hostable, and creators can collaborate anywhere.
- All improvements to Floorplanner recipes and features become available to anyone.
- Floorplanner can be bundled with its own solutions as an integrated design tool.
- Floorplanner project uses itself for development, to evolve its own Solution Blueprint.

## Vision of a Peopleverse

Solidground has a vision for the Fediverse. The Fediverse is this rather technical concept with servers, instances, federation, apps and timelines. But both Social and Social networking has nothing to do with technology, which can only ever be supportive. The current Fediverse is modeled after corporate Social Media and that limits our perception of what it can be.

The Peopleverse is the 'social fabric' that the Fediverse enables, where online activity ties seamlessly into our daily lives and is in harmony and connection with the real world and human values. Envisioning the Peopleverse enables dreaming of what could be possible and unleash creativity and innovation to make it come true.

{: .more}
> Read our blog [➥ Let's Reimagine Social](https://fedi.foundation/2022/09/social-networking-reimagined/) for more introduction.

## Learn from our showcases

### Social Coding IdeationHub

The IdeationHub is a proof of concept that is part of the [Social Coding Movement](https://coding.social/ecosystem/projects/ideationhub/) and will be developed alongside and in parallel to Taskweave, Floorplanner and Solidground. It will serve as a reference implementation, and be a production-ready social experience.<br>[➥ More about IdeationHub ..](/experiences/ideationhub)


