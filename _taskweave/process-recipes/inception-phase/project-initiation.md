---
layout: default
title: Project Initiation
parent: Inception Phase
nav_order: 2
permalink: process/inception-phase/project-initiation
---

# Project Initiation
{: .fs-9 .no_toc }

Set up the prerequisites and organization structure so that the project can get underway.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary