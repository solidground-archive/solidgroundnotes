---
layout: default
title: Behaviour Tests
parent: Domain Modeling
nav_order: 3
permalink: process/domain-modeling/behaviour-tests
---

# Behaviour Tests
{: .fs-9 .no_toc }

Describe use case behaviour in common language and test it against the codebase.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary