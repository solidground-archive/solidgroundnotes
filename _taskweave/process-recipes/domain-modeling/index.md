---
layout: default
title: Domain Modeling
parent: Discover your needs
has_children: true
nav_order: 2
permalink: process/domain-modeling
---

# Domain Modeling
{: .fs-9 .no_toc }

Evolve a robust architecture that can meet requirements.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Process Recipes
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

Here you find an overview of useful recipes for the Domain Modeling discipline.

## Event Storming

TODO

#### Learn more
{: .no_toc}

## Behaviour Tests

TODO

#### Learn more
{: .no_toc}
