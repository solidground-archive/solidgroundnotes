---
layout: default
title: Domain Glossary
parent: Domain Modeling
nav_order: 1
permalink: process/domain-modeling/glossary
---

# Domain Glossary
{: .fs-9 .no_toc }

Define and explain the common terminology that everyone uses, to ensure a common understanding exists.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary