---
layout: default
title: Discover your needs
has_children: true
nav_order: 2
permalink: process/recipes
---

# Delve into Taskweave
{: .fs-9 .no_toc }

Discover your client's needs, how to address them best and improve your own process.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> This a _staging area_. Gradually **process recipes** below will be adopted in [Floorplanner](/floorplanner/design) with added automation support, to help you be most productive in your design process.

## How this works

Below we collect a broad range of Recipes representing tasks related to Social Experience Design. Choose the recipe that matches your needs to find detailed instructions and background information.

## Project phases

### Inception phase

In the inception phase focus is to **establish a healthy basis** for the Free Software project.

#### Project Initiation

| | Set up the basic project structure and communication channels that will be used. |
| :--- | :--- |
| **When to use** | - At the start of your new social experience design. |
| **Time spent** | About half a day to one day. Depends on process preferences and project size. |
| | [Get Started](inception-phase/project-initiation){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](inception-phase#project-initiation){: .btn .fs-3 .mb-4 .mb-md-0 }  |

#### Shared Vision

| | Get a high-level understanding of problem statement and the desired solution. |
| :--- | :--- |
| **When to use** | - At the start of your new social experience design.<br>- When adding new sub-domains to your social experience. |
| **Time spent** | About 2-4 hours. Continue to keep up-to-date afterwards. |
| | [Get Started](domain-modeling/shared-vision){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](inception-phase#shared-vision){: .btn .fs-3 .mb-4 .mb-md-0 } |

## Disciplines

### Domain Modeling

Discipline to gain clear **insight and knowledge** of functional areas in order to:

- Find **client needs** to be addressed in the social experience.
- Gain **common understanding** and adopt a **shared language** to use.
- Use [**domain driven design**](/process/focus/domain-driven-design) (DDD) to **elaborate features** to deliver.

#### Domain Glossary

| | Common terminology that all stakeholders must use throughout the project. |
| :--- | :--- |
| **When to use** | - Keep up-to-date during the entire project lifecycle. |
| **Time spent** | Minutes. Update as soon as common terminology is settled on. |
| | [Get Started](domain-modeling/glossary){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](domain-modeling#glossary){: .btn .fs-3 .mb-4 .mb-md-0 } |

#### Event Storming

| | Collaborative workshop to get a shared understanding of important needs. |
| :--- | :--- |
| **When to use** | - At the start of your new social experience design.<br>- When adding new sub-domains to your social experience. |
| **Time spent** | Initial session 4-6 hours. Multiple refinements afterwards. Keep up-to-date. |
| | [Get Started](domain-modeling/event-storming){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](domain-modeling#event-storming){: .btn .fs-3 .mb-4 .mb-md-0 } |

### Analysis & Design

#### Architecture Decisions

| | Track the status and outcome of decisions that affect the architecture design.  |
| :--- | :--- |
| **When to use** | - For any architecture decision with major impact on the implementation.<br>- To support non-functional requirements of the software.<br>- To ensure consensus about architecture direction in the project team. |
| **Time spent** | Decisions are preceded by a [Spike](/process/recipes#spikes). Then one hour to document the decision. |
| | [Get Started](analysis-design/architecture-decisions){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](analysis-design#architecture-decisions){: .btn .fs-3 .mb-4 .mb-md-0 }  |

#### Technology Radar

| | Keep track of technologies, pros and cons, and decisions to adopt or postpone. |
| :--- | :--- |
| **When to use** | - Keep up-to-date during agile development, recording the outcome of Spikes. |
| **Time spent** | About half an hour to update. Decisions to `Adopt` must be preceded with a [Spike](/process/recipes#spikes). |
| | [Get Started](analysis-design/technology-radar){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](analysis-design#technology-radar){: .btn .fs-3 .mb-4 .mb-md-0 }  |

#### Behaviour Tests

| | Write feature descriptions for use cases and scenario's that are tested against code.  |
| :--- | :--- |
| **When to use** | - Throughout the project while elaborating and refining the domain.<br>- To discuss the behaviour of a social experience with Client stakeholders.<br>- To clarify to Creator stakeholders what functionality should be built. |
| **Time spent** | 1-2 hours per Test, depending on functionality and feature complexity. |
| | [Get Started](analysis-design/behaviour-tests){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](analysis-design#behaviour-tests){: .btn .fs-3 .mb-4 .mb-md-0 }  |

### Implementation & Test

#### Techstack

| | Frameworks and libraries used by the codebase, and rationale for choosing them.  |
| :--- | :--- |
| **When to use** | - During agile development whenever dependencies are added. |
| **Time spent** | About one hour. High impact choices are preceded with a [Spike](/process/recipes#spikes). |
| | [Get Started](implementation-test/techstack){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](implementation-test#techstack){: .btn .fs-3 .mb-4 .mb-md-0 }  |

#### Spikes

| | Timeboxed investigations of architecture decisions and technology choices.  |
| :--- | :--- |
| **When to use** | - During agile development as iterations are planned on the project board. |
| **Time spent** | About half a day, up-to a full iteration, depending on impact and complexity. |
| | [Get Started](implementation-test/spikes){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](implementation-test#spikes){: .btn .fs-3 .mb-4 .mb-md-0 }  |

#### Scaffolding

| | Transform Floorplanner Blueprint designs into boilerplate projects and tailored code.  |
| :--- | :--- |
| **When to use** | - To generate a brand new social experience project.<br>- To generate code files for an additional domain.<br>- To generate code files for selected parts of a domain diagram. |
| **Time spent** | Minutes. |
| | [Get Started](implementation-test/scaffolding){: .btn .btn-primary .fs-3 .mb-4 .mb-md-0 .mr-2 } [Learn More](implementation-test#scaffolding){: .btn .fs-3 .mb-4 .mb-md-0 }  |

