---
layout: default
title: Architecture Decisions
parent: Analysis & Design
nav_order: 3
permalink: process/analysis-design/architecture-decisions
---

# Architecture Decisions
{: .fs-9 .no_toc }

Keep track of all major decisions that affect the architecture.
{: .fs-6 .fw-500 }

{: .note}
> Adjust this pattern to your preferences. See: [Architecture Decision Records](https://adr.github.io/) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary

| | Track the status and outcome of decisions that affect the architecture design.  |
| :--- | :--- |
| **When to use** | - For any architecture decision with major impact on the implementation.<br>- To support non-functional requirements of the software.<br>- To ensure consensus about architecture direction in the project team. |
| **Participants** | - Person(s) investigating the decision.<br>- People affected by the decision (stakeholders). |
| **Requirements** | - Decisions are preceded by a [Spike](/process/recipes#spikes) to gather background information.<br>- Inform stakeholders, process their feedback, involve them in decision-making. |
| **Duration** | About one hour to document the decision. |
| **Expected outcome** | An architecture decision has been made. |

[➥ I need more info](/process/analysis-design/#architecture-decisions){: .btn .fs-5 .mb-4 .mb-md-0 }

## Preparation

1. **Template**: Determine the markdown template to use for your ADR's.
  - Template should be _lean_. Lower the barrier of creation and maintenance work involved.
  - Template should be _informative_. Easy to parse by readers.
2. **Location**: Determine the location where the template and all ADR's are located.
  - Best: In the version-controlled codebase, e.g. in a `/docs/adr` folder.
  - Alternative: In the project's `Docs` website if architecture is documented there.
3. **Tools**: Configure tools to help support the ADR documentation procedure.
  - For example, Issue and PR templates, issue tags, specialized [ADR tools](https://adr.github.io/#decision-capturing-tools) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>
4. **Procedure**: Document the procedure for creating ADR's.
  - For example, in the project's `README.md` or `CONTRIBUTING.md` or in the docs.

## Usage guidelines

Following guidelines are helpful to consider:

- ADR's are brief, 1 to 2 pages.
- ADR's are listed on the project backlog / issue tracker.
- ADR's are reviewed before text is committed.
- ADR's are uniquely numbered (e.g. `ADR 001`).
- ADR filenames are consistent, can be sorted (e.g. `adr-001-track-architecture-decisions.md`)

## Procedure

This procedure assumes ADR creation is preceded by a [Spike](/process/implementation-test/spikes).

1. Create an Issue in the project tracker of the repository affected by the decision.
  - Add the `spike` label.
  - Prefix title with `ADR:` followed by ADR Title.
  - Briefly describe the ADR Summary.
2. Create a Pull Request based on the ADR Template.
  - Prefix the PR with `WIP:` as long as the Spike isn't completed.
  - Reference the Spike issue in the PR description.
3. Update information based on work done on the Spike.
  - Assign the Spike issue to the people in charge of elaborating.
  - Use the Spike issue as location for discussion, feedback collection.
  - Update the text of the ADR Pull Request as needed.
4. Merge the ADR Pull Request when it passes review of affected stakeholders.
  - Merging the PR closes the Spike issue.
  - Update the ADR decision table with the outcome of the decision.

### Variation: Pull Requests as Spikes

Step 1. and 2. can be combined, whereby the Pull Request becomes the Spike, instead of having a separate issue for it in the issue tracker. Beware when doing so, that the PR should be able to be tracked as-if part of the backlog, and allows for ongoing discussion to take place. 

{: .hint}
> The [Forgejo](https://forgejo.org) code forge supports this process, and Groundwork uses this variation as follows:
>
> - Top-level [Architecture](/groundwork/project/architecture/#architecture-decisions) page in the Docs contains a table listing all ADR's.
> - Individual ADR files are stored in `/docs/adr` in the [Groundwork repository](https://codeberg.org/solidground/groundwork) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>
> - ADR Pull Request is tracked on a Kanban board in the same repository.
>   - For example PR for [ADR-001](https://codeberg.org/solidground/groundwork/pulls/2) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>
>   - PR is tracked on the [Development board](https://codeberg.org/solidground/groundwork/projects/2768) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>
