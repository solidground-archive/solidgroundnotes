---
layout: default
title: Analysis & Design
parent: Discover your needs
has_children: true
nav_order: 3
permalink: process/analysis-design
---

# Analysis & Design
{: .fs-9 .no_toc }

Evolve a robust architecture that can meet requirements.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Process Recipes
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

Here you find an overview of useful recipes for the Analysis & Design discipline.

## Technology Radar

A technology radar helps introduce new technology in a controlled manner. In the IT world new and trendy technologies emerge quickly and there's a multitude of tools and frameworks that gets technologists very excited. A tech radar allows for making informed choices and become opinionated and strategic in the technical direction that is followed. Four categories of technology are tracked, and for each entry in the radar an investigation determines the adoption status, which can be revisited later. [➥ Apply this Recipe](technology-radar)

#### Learn more
{: .no_toc}

- [Thoughtworks: Build your own radar](https://www.thoughtworks.com/radar/byor) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>

## Technology Standards

#### Learn more
{: .no_toc}

## Architecture Decisions

Track a historical record about architecture decisions for future reference. For each decision an Architecture Decision Record (ADR) documents details about the software system's architecture. It captures context, problem, proposed solution, and reasoning behind the decision. ADRs aim to improve communication, ensure well-informed and well-documented decisions. ADR's are created by the team responsible for the decisions, and are immutable once the decision is made. Future changes lead to creation of a new ADR that supercedes the old one. [➥ Apply this Recipe](architecture-decisions)

#### Learn more
{: .no_toc}

- [Architecture Decision Records (ADR's)](https://adr.github.io/) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>
- [Discuss Social Coding: Tracking architecture decisions](https://discuss.coding.social/t/tracking-architecture-decisions/313) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>

## Techstack

#### Learn more
{: .no_toc}

