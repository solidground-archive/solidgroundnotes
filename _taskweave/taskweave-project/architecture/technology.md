---
layout: default
title: Technology choices
parent: Architecture
nav_order: 2
permalink: taskweave/project/architecture/technology
---

# Taskweave Technology Choices
{: .no_toc }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Introduction

{: .todo}
> Turn explanation into a process recipe.

{: .hint}
> Detailed information about [Using Technology Radars](#) can be found in our process recipe.

This page keeps track of major decisions that relate to Taskweave technology stack. Here we explore libraries and frameworks that are candidates to adopt in support of various architecture concepts and requirements. Decisions are ordered in reverse chronological order (Newest-first) and have states. They represent a [Technology Radar](https://www.thoughtworks.com/radar/faq-and-more). It has one additional 'Open' state, meaning that an investigation, called a Spike, is planned.:

<div markdown="1">
Adopt
{: .label .label-green }

Trial
{: .label .label-blue }

Assess
{: .label .label-yellow }

Hold
{: .label .label-red }

Open
{: .label .label-grey }
</div>

## Decision Log

### Adopt Elixir PETAL technology stack

Adopt
{: .label .label-green }

The [PETAL](https://changelog.com/posts/petal-the-end-to-end-web-stack) stack is a popular combination of frameworks and libraries that have proven to be very powerful and flexible. It consists of:

- [**P**hoenix](https://www.phoenixframework.org/): Leading web framework for Elixir.
- [**E**lixir](https://elixir-lang.org/getting-started/introduction.html): Elixir server-side rendering code.
- [**T**ailwind](https://tailwindcss.com/): CSS framework to mark up HTML with semantic classes.
- [**A**lpine](https://alpinejs.dev/): Minimal framework for composing JS behaviours in markup.
- [**L**iveview](https://github.com/phoenixframework/phoenix_live_view): Real-time UX with server-rendered HTML.

Elixir and Phoenix Liveview were already part of the stack. Tailwind was a great experience in migration from Bootstrap for [fedi.foundation](https://fedi.foundation) and is intuitive to use for non-CSS experts.