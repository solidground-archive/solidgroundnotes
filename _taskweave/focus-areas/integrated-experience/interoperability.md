---
layout: default
title: Interoperability
parent: Integrated Experience
nav_order: 2
permalink: process/focus/integrated-experience/interoperability
---

# Interoperability
{: .no_toc }

<br>

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## What causes Ad-Hoc Interoperability?

Here we'll distill some of the findings of investigation as they relate to federated app development moving towards ad-hoc interoperability.

### Social aspects

- Vast majority of federated apps start as hobby projects on a [serendipitous model](https://matt.life/writing/the-asymmetry-of-open-source), where developers aren't reliant for their income.
- Developers are in it to satisfy their own passion for the Fediverse and the underlying technology. A tech focus prevails.
- Developers often lack both the interest and the social skills to develop inclusive [communities of action](https://en.wikipedia.org/wiki/Community_of_action) around their projects.
- Individualistic and egocentric tendencies, harsh criticisms and past conflicts, have harmed the spirit of healthy collaboration.

### Cultural aspects

- Fedizens are a very diverse grassroots group of people, having strong values and very strong opinions.
- Overall the culture has anarchistic characteristics, strong resistence to structures of authority and power.
- Positive effect is a peer pressure to align with morals and ethics that are needed to affect positive change.
- Negative effects are being overly critical, canceling prematurely, infighting, factions, purity spirals, hampering broad collaboration.

### Technical aspects

- There is strong lack of vision of what the Fediverse can be, and how that can be achieved.
- Technology of the Fediverse is complex, opaque, and knowledge is fragmented, undocumented and incomplete.
- Most technology experts aren't advocating, onboarding, have moved on, have no time, aren't easily accessible.
- Innovation remains stuck in R&D phase of the [technology lifecycle](https://en.wikipedia.org/wiki/Technology_life_cycle) and there is little [diffusion of innovations](https://en.wikipedia.org/wiki/Diffusion_of_innovations).
  - Individuals making progress directly implement innovations as app features, but do not help further adoption.

### Organizational aspects

- There exists no organization that dedicates to the substrate of the Fediverse that yields productive results.
- Such organization would need an amount of dedication that is very hard to achieve in existing cultural and social context.
- Funding activities of substrate formation may provide good incentives, but most funding is solely available at app level.
  - Most grants fund specific projects and only related to aspects of technical innovation.

### Process aspects

- Even if a developer wants to contribute their innovation progress to substrate formation, there's hardly any process to do so.
- There are no conventions, no common documentation standards, no people to hand things over to, no structure.

## What are the consequences of Ad-Hoc Interoperability?

- Broad interoperability becomes ever more complex, integrations between apps become harder.
- Incompatibilities and 'solution flavours' become more engrained over time, with growing frictions to improve.
  - Backward-compatibility, not-invented-here, dominance (post-facto interop), lack of time / interest (past station, done deal).
- Technology landscape is stuck in early adoption stage. The technology adoption cycle is broken.
  - Replaced by individual app adoption cycles that shape the future of the Fediverse.
- Popular, well-productized and dominant apps determine perception, expectations, vision and evolution of the Fediverse
  - E.g. many outsiders think that Mastodon, a microblogging app, is equivalent to the Fediverse.
  - Even fedizens think of Fediverse as a microblogging medium, and federation support as adding microblogging features.

There are likely more subtle, secondary effects in play, that reinforce other detrimental aspects to substrate formation and create sort of a vicious cycle:

- Implementing federation support feels more like a struggle to overcome than an achievement to celebrate.
- Implementations feels like having added low-level protocol plumbing, rather than now being part of a disruptive movement.
- This being an individuals struggle, part of app development with little community help, it is perceived as an app-related achievement only.
- The celebration of Fediverse support is restricted by highlighting the bullet list of social features that were added.
- What is not part of the celebration is what is now enabled. Being part of something much bigger, and utterly exciting.

After having added federation support:

- Focus remains solely on the app (app silo) and advocacy - to the extent it is part of project activity - is on _application adoption_.
- There's little to no focus on _technology adoption_ and visionary aspects of federation support ("Social Networking Reimagined").
- If integrations to other apps are established (on 1-by-1 basis mostly) they are just extra bullet points in the feature list.

→ **The technology adoption cycle is inherently broken**. The whole ecosystem can't flourish, and most early adopters move on again.

## Ad-hoc Interoperability Examples

### Example 1: Groups standardization

{: .highlight}
> **Substrate → Implementations**. This example is about the effort to use the substrate, community collaboration, to come to standardization across implementations.

We will focus on the discussion that took place in the SocialCG and SocialHub. Group support, one of the Actor types in ActivityStreams is an example where early on there was an attempt to come to a standard for interoperability across applications. 

#### Collaboration activity

- 2019-10-12: [@trwnh](https://socialhub.activitypub.rocks/u/trwnh) first mentions [Accept/Reject Join semantics](https://socialhub.activitypub.rocks/t/signaling-side-effects-asynchronously-by-generalizing-accept-reject/125) for closed membership Groups, but it is not discussed further.
- 2019-12-16: [@datatitian](https://socialhub.activitypub.rocks/u/datatitian) announces [Guppe Groups](https://socialhub.activitypub.rocks/t/guppe-groups-a-new-activitypub-app-for-decentralized-social-groups/408) with no real follow-up discussion.
- 2019-12-22: [@nutomic](https://socialhub.activitypub.rocks/u/nutomic) of Lemmy wonders how to implement Webfinger for groups.
  - In response [@wakest](https://socialhub.activitypub.rocks/u/wakest), not involved with Mastodon, links to [Mastodon PR #12071 Add basic support for group actors](https://github.com/tootsuite/mastodon/pull/12071).
  - In [mastodon#12071](https://github.com/tootsuite/mastodon/pull/12071) there's cross-reference [from Misskey](https://github.com/misskey-dev/misskey/pull/5534) who (AFAICS) still have no full native group support.
  - [@grishka](https://socialhub.activitypub.rocks/u/grishka) of Smithereen announces plans to support Groups in the future.
  - [@nedjo](https://socialhub.activitypub.rocks/u/nedjo) of Drupal, and [@diogo](https://socialhub.activitypub.rocks/u/diogo) of GNU Social are mingling in the discussion and doing tests.

→ **To date there's no still conclusion on Webfinger for Groups now, and there are incompatible implementations.**

- 2020-02-18: [@grishka](https://socialhub.activitypub.rocks/u/grishka) makes a proposal for Groups before starting Smithereen implementation, to discuss first.
  - [@lanodan](https://socialhub.activitypub.rocks/u/lanodan) of Pleroma remarks _"We more or less already have [Groups], see Peertube Channels"_. Mentions addressing too.
  - Indeed PeerTube started implementing [VideoChannel as Group actor](https://github.com/Chocobozzz/PeerTube/commit/50d6de9c286abcb34ff4234d56d9cbb803db7665#diff-4cb0a95909e27ca80b713fc44fe0e0a45445b55c8ec3232062b60feaf04efd6b) back in 2017-12-19 already.
  - [@heluecht](https://socialhub.activitypub.rocks/u/heluecht) mentions briefly how Friendica handles Groups, no links provided.
  - [@grishka](https://socialhub.activitypub.rocks/u/grishka) points to Mastodon bots' similarity, but that "full-featured" groups will not be 100% compatible with microblogging.
  - [@macgirvin](https://socialhub.activitypub.rocks/u/macgirvin) implemented Friendica group support, explains how they are implemented in Zap, highlights important features.
  - [@AceNovo](https://socialhub.activitypub.rocks/u/AceNovo) of [YAAPS](https://gitlab.com/swift2plunder/yaaps) suggests  using `Organization` for formal, organized Groups, and `Group` for more informal uses.
  - Thread ends with an unanswered question by [@lanodan](https://socialhub.activitypub.rocks/u/lanodan) at 2020-02-20 related to starting Pleroma Groups implementation.
- 2020-03-30: [@ngerakines](https://socialhub.activitypub.rocks/u/ngerakines)  of Tavern asks feedback and comments on their [Groups implementation](https://socialhub.activitypub.rocks/t/groups-implementation/591).
  - Starting off with a good description of Group side effects, and with 83 posts the discussion is still ongoing.
  - Discusses semantics to express "user intent" vs. also "capture the outcome", with [@cjs](https://socialhub.activitypub.rocks/u/cjs) saying the form are most relevant.
  - [@cjs](https://socialhub.activitypub.rocks/u/cjs) makes important points on semantics and "intent" in [this comment](https://socialhub.activitypub.rocks/t/groups-implementation/591/6?u=aschrijver) and the [follow-up](https://socialhub.activitypub.rocks/t/groups-implementation/591/8?u=aschrijver) to that.
  - [@trwnh](https://socialhub.activitypub.rocks/u/trwnh) points out that Follow and Join semantics can be used to express the same action to become a Group member.
  - [@ngerakines](https://socialhub.activitypub.rocks/u/ngerakines) posts a [FEDERATION.md](https://gitlab.com/ngerakines/tavern/-/blob/master/FEDERATION.md) for Tavern, and [@yvolk](https://socialhub.activitypub.rocks/u/yvolk) of AndStatus points out that Join and Follow are different actions.
  - After ton of information exchange, both on-topic + off-topic, and no elaborations, the thread loses activity at 2020-07-31.
- 2020-10-02: [@sl007](https://socialhub.activitypub.rocks/u/sl007) creates a [summary of open Group topics](https://socialhub.activitypub.rocks/t/how-to-use-groups/1005) for the ActivityPub Conference in a Birds on a Feather session.
- 2021-08-11: [@dansup](https://socialhub.activitypub.rocks/u/dansup) announces working on Group support in Pixelfed, starting new discussion on [Standardizing AP Groups](https://socialhub.activitypub.rocks/t/standardizing-on-activitypub-groups/1984).
  - Discussion is still ongoing with 58 comments, and also the [Groups implementation](https://socialhub.activitypub.rocks/t/groups-implementation/591) topic became active in parallel / overlap.
- 2022-02-08: [@sl007](https://socialhub.activitypub.rocks/u/sl007) starts to organize a number of [Developer Meetings for "Groups"](https://socialhub.activitypub.rocks/t/developers-meeting-2-groups/2302).
  - As input a summary of group-related information has been created.
  - One month later several meetings were held, but not reported on. Who joined, what was discussed, was it useful?
- 2022-02-10: [@nutomic](https://socialhub.activitypub.rocks/u/nutomic) of Lemmy requests [feedback from different apps on their Groups implementation](https://socialhub.activitypub.rocks/t/help-improving-federation-between-lemmy-and-other-projects/2308).

#### Observations

- Group implementations started sprouting everywhere, at different times, with different objectives, and evolving differently.
- Getting overview which projects are interested / working on Groups, what their status is, and what they implemented, is hard.
- Groups where discussed by many people on SocialHub who just happened to notice and take time to discuss them.
- A ton of useful information was provided along the way, all from different perspectives, but no one took the lead to organize it.
- App developers mostly have vague requirements and roadmap for their own federated projects, which aren't clear in the discussion.
- Interaction is a mix of openness to collaboration, defensiveness of own approach, and critical or hostile barbs to other projects.
- Especially in broader Fediverse contexts, there's unproductive technical ideological opinion/warfare, criticism of past decisions.
- Group capabilities are considered in narrow domain, mostly Microblogging, and features inpsired from tradtional social media.
- Discussions are a chaotic mix of design talk, deeply technical matters, edge cases, capabilities, features, constraints, etc.
- In discussions questions are not tracked, answers and open issues not recorded, and many inquiries not followed-up on.
- No efforts were made to separate use cases, domain design, vocabulary design, business logic, protocol mechanisms, etc.
- Importantly most of Group implementation happens elsewhere, in backchannels, 1-to-1 settings, different code bases / issue trackers.

→ **After nearly 2.5 year of discussion merely distilling the history of what was discussed before is hours of work.**

#### Observations related to Solidground

- The concepts being discussed, and where consensus and standardization is sought, does not map 1-to-1 to domain-driven design.
  - Some aspects such as finding Groups (Webfinger) or dealing with large groups (avoiding `sharedInbox`) are protocol-level.
  - Core concept of Groups, membership, join, leave, invite, etc. are at more granular level than typically a Bounded Context would be.
- There's the core concept of "groups + membership" and then there's many different additional capabilities of groups.
  - Some refer to a different Group model altogether, e.g. 'Relay Groups' versus "real groups" (as yet not properly named).
  - Some refer to additional capabilities that can be combined or stacked on top of the core model.
  - Some refer to other concepts that should be modeled independently (e.g. Roles, Privileges).
  - Some refer to concepts / mechanisms that should be further generalized (e.g. [Unbound Actor](https://socialhub.activitypub.rocks/t/fep-8485-unbound-actor/2200), [Actor Relationships](https://socialhub.activitypub.rocks/t/standardizing-on-activitypub-groups/1984/32?u=aschrijver))
- There's no common terminology, part of Ubiquitous Language, to serve common understanding, and no efforts to create one.
- Conceptually in model designs you find correspondences to OO language features, such as Inheritance, Composition, Aspects.
  - Inheritance relationship to derive from a primitive base type in ActivityStreams, and further specilizations of these.
  - Composition and/or Aspects to indicate additional capabilities that are associated with a particular domain concept.
- There are no modeling practices, design methods, well-defined procedures or document templates to help bring structure.
 
#### Conclusion

A couple of assumptions result from the above:

- Most likely the perception will be that SocialHub "can get nothing done" and that mingling in the discussions is not worth the time.
- Most likely practical app developers would rather skip SocialHub discussion and "just-do-it", if they want to make progress.
- Most likely popular, fast-moving apps (many contributors) will be early adopters, and likely lead via post-facto interoperability.
- All the different requirements that federated apps have will likely result in a patchwork of Group flavours

→ **Ad-hoc Interoperability will likely further increase over time, and the ecosystem will become more unattractive for newcomers**.

#### Additional resources

Guppe Groups JSON-LD example:

```json
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ],
  "id": "https://a.gup.pe/u/activitypub",
  "type": "Group",
  "inbox": "https://a.gup.pe/u/activitypub/inbox",
  "publicKey": {
    "id": "https://a.gup.pe/u/activitypub#main-key",
    "owner": "https://a.gup.pe/u/activitypub",
    "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA1Q+a63e8RYkEyuxj2S9q\nwcNbjYglNcEhT/hfIsaXwK9/JY17VjxihHlBzyR5jn64Nf2j+6Th3MX9j+bk6eTo\n7NqupTE2hQ5PgAuC0ywdwFve0cDKyZ2i9gxq90sHzvxdyMK9nlnwt3toZk+7ihRp\n/V/IG0ahMHb9kppJf8Aoin+N2s/GqdrYgWd2DxdEmXkNRCywywKQX257uGyB9Hnd\nBq37Bg0An8GVTLAOvQFr95kip5f8P8pM7U8glIoC8icVd+fIoCDg7HJiT0hszLRT\nQuO6MsYb/2AOILFdQuZ+y6v9NLc2hkGcvd2L8pdF2wSqgowJsawu6zQKXwy05R5V\nY1ZhjuVThYNnthgjat7yABBYHqk0skU2xorjeMpQq3BnHVOsQQjJEp2kRKcKlPGz\nyVgQzlmuInZt8v5B0PZ5o2WqyqJeaSIG57LD8ak1v2wyHccXcCPHGKorrKCCS4eR\nrZG997Ox/ICKv+k6slwuLCgBnLXwXu3cXB/dTl61Vblr0VCh0uIRl5bIUqkA2BiB\n+/8ZlW2/SXUN3uvhcnrIs4NzCq40hKQU1vws/gCz1ASjCMpoPTFOFaAxwMJv2wUj\nUcwBYwsT4RqZAO089RPc0Niy5GcthN5B1k+R4FiLP2x/WvYcN0KxbKjk4rc59ycQ\nGmLNgiWSccYH/xeyeRjFaCcCAwEAAQ==\n-----END PUBLIC KEY-----\n"
  },
  "followers": "https://a.gup.pe/u/activitypub/followers",
  "following": "https://a.gup.pe/u/activitypub/following",
  "icon": {
    "type": "Image",
    "mediaType": "image/jpeg",
    "url": "https://a.gup.pe/f/guppe.png"
  },
  "liked": "https://a.gup.pe/u/activitypub/liked",
  "name": "activitypub group",
  "outbox": "https://a.gup.pe/u/activitypub/outbox",
  "preferredUsername": "activitypub",
  "summary": "I'm a group about activitypub. Follow me to get all the group posts. Tag me to share with the group. Create other groups by searching for or tagging @yourGroupName@a.gup.pe"
}
```

Guppe is based on [activitypub-express](https://github.com/immers-space/activitypub-express) written in Javascript.

### Example 2: Social video platforms

{: .highlight}
> **Implementation → New implementation**. This example is about an application that has another similar and well-established application as a starting point.

We look at Peertube, which was one of the first apps entering a new domain and inventing ActivityPub extensions to represent it. Much later Owncast was implemented, and - though it is in a different domain, real-time video streaming - we look how Owncast currently relates to Peertube.

#### Peertube observations

Remarks on the ActivityPub formats found in the examples:

- [Object](https://www.w3.org/TR/activitystreams-core/#object) `"type"` property not compatible: _"When an implementation uses an extension type that overlaps with a core vocabulary type, the implementation MUST also specify the core vocabulary type."_
  - `PlayList` overlaps `Collection` (or `OrderedCollection`, but that cannot be determined from the JSON-LD).
- Schema.org properties are used to extend objects, where they were likely matches to desired semantics.
  - **Question**: Does this reuse add value? Most properties can only be understood if you understand the surrounding peertube semantics.
  - [`sc:datePublished`](https://schema.org/datePublished) (redefined as `"originallyPublishedAt"`, and described as _"Date of first broadcast/publication"_) is used on Video objects to distinguish from ActivityStream's `"published"` date (described as _"The date and time at which the object was published"_). In the Video example above the datePublished is 6 days later than publication of the as:Video object. The exact meaning of both dates isn't clear. Was the `as:Video` created but only federated 6 days later?
  - [`sc:license`](https://schema.org/license) (redefined with typo as `"licence"` for some reason, backwards-compatibility?) contains `"name"` and `"identifier"` properties of [CreativeWork](https://schema.org/CreativeWork), with app-specific values and insufficient information for other apps to understand the license used.
  - Video pages contain [`VideoObject`](https://schema.org/VideoObject) definition in HTML metadata. Might have been an opportunity to reflect that in AS data as well.
  - Schema.org is used to indicate the data type of properties. Used are: `sc:Number`, `sc:Boolean`, `sc:Text`
  - Uses [`"sc:inLanguage"`](https://schema.org/inLanguage) to explicitly set a language, does not use AS language map or set [default language in `@context`](https://www.w3.org/TR/activitystreams-core/#naturalLanguageValues).
- Properties are used that do not exist in the ActivityStreams namespace.
  - Some are post-facto standards to ensure compatibility with Mastodon, such as `"as:sensitive"` and `"as:HashTag"`.
  - Peertube also introduces new ones in this namespace: `"as:dislikes"` and `"as:comments"` collection URI's.
  - Or adds collection URI's that actually belong to `Actor` to the `Video` object, such as `"as:shares"` and `"as:likes"`.
- Video `'url"` property has a [HLS](https://en.wikipedia.org/wiki/HTTP_Live_Streaming) playlist URL (only if HLS transcoding is enabled), that uses a bit of a contrived construct to supply additional information. Its `"tag"` array consists of related links. A custom link type may have been a better choice.
- At Peertube namespace `https://joinpeertube.org/ns#` no JSON-LD file can be retrieved (as with AS and Security namespaces)

Remarks on the ActivityPub documentation:

- The documentation, while minimal, is useful. But it is not _entirely_ up-to-date, and can be made much more accessible from a "substrate formation" perspective.
  - Create a better separation of app-specific and generic ActivityPub vocabulary / domain-specific information.
  - Provide more detail on the heuristics that are used. E.g. "`Follow{Actor}` is successful once `Accept{Follow}` is received".
  - Better template and formatting might be used, so e.g. it represents more as a specification document similar to AS/AP specs.
- Does not follow [informal convention for a FEDERATION.md](https://socialhub.activitypub.rocks/t/documenting-federation-behavior-in-a-semi-standard-way/453) file maintained in the root of the codebase.
  - Which is understandable if ActivityPub is already in the docs. [(Federated) Murmurations for FEDERATION.md](https://socialhub.activitypub.rocks/t/improvement-to-federation-md-convention-murmurations/1573) may be a solution.
- There's a confusion in that the [Create](https://docs.joinpeertube.org/api-activitypub?id=create) activity quotes text from the C2S part of the specification: _"The Create activity is used when posting a new object. This has the side effect that the object embedded within the Activity (in the object property) is created."_
  - This implies a command, while in S2S the Create activity indicates something that already happened, i.e. an event.

Remarks on the 'federation domain model':

- When Strategic DDD were used, there was likely a somewhat different division in multiple Bounded Contexts
- Opportunities to slice domain concepts into different more reusable concepts:
  - `CacheFile` may have been part of a Caching vocab, and available for general use in any caching scenario.
  - `pt:support` may have been part of a Monetization vocab, `sc:license` part of a Attribution/Licensing vocab.
  - HTTP Live Streaming might have been defined in a separate vocab definition, or part of a larger "Social Video" vocab.
- Instead of marking live streams with the `"isLiveBroadcast"` boolean property, using a separate type may be better.
  - Like a [`BroadcastEvent`](https://schema.org/BroadcastEvent) as defined by schema.org and a `Video` once stored/published.
  - Or `BroadcastEvent` (or something similar) as a secondary item in the AS Object `"type"` array.

Remarks on substrate / community interaction:

- Peertube was first in Social Video Platform domain --> early adopter 'invents' the domain.
- Project maintainer [Chocobozz](https://socialhub.activitypub.rocks/u/chocobozzz/summary) joined SocialHub in October 2019, last seen in February 2020, with 30min. read time.
  - Invited by [Hellekin](https://socialhub.activitypub.rocks/u/how) they had no forum interaction, despite having a [Peertube](https://socialhub.activitypub.rocks/c/software/peertube/27) software category.
- Peertube has its own [Peertube category](https://framacolibri.org/c/peertube/38) on Framasoft's forum with a total of 4105 users (SocialHub has 614 users).
  - There are 43 topics with 'ActivityPub' as keyword, 31 with 'Fediverse', 36 with 'federated'. Half of topics are in French.
- Peertube has a great [news archive](https://joinpeertube.org/news-archive), and posts on [Framasoft blog](https://framablog.org), but this is mostly focused on product features / roadmap.
- In the codebases issues are labeled under [Federation component](https://github.com/Chocobozzz/PeerTube/issues?q=is%3Aopen+is%3Aissue+label%3A%22Component%3A+Federation+%3Aferris_wheel%3A%22) (18 open, 54 closed), some with good interaction on them.
- ActivityPub docs are a starter, but figuring out how AP is implemented requires backtracking issues, studying codebase.
- **Overall conclusion**: ActivityPub implementation is application specific. Project process is: file issue, discuss, PR, merge.
  - Substrate formation for Social Video Platforms did not occur, and a newomer will have to apply Ad-hoc Interoperability.

#### Owncast federation support

We will not have a detailed look at how [Owncast](https://github.com/owncast/owncast) - as a self-hosted live video and web chat server - implemented ActivityPub,   but rather how they might have benefited from Peertube paving the way, as well as future interoperability with Peertube.

As mentioned in the [introduction video](https://yewtu.be/watch?v=aeVvS0E-z3g) on federation support Owncast deliberately does not want to offer a 'microblogging' experience. Video casts will be announced, and people can forward those to their friends, but there will not be "a bi-directional social networking vibe" as Gabek, the developer, says. The goals is "to encourage the live streamer" and key question is "What _actually_ benefits the live streamer?". Federation strives to increase engagement, and only Liking, Boosting and Following are enabled, while those activities are visible in the live chat.

In a [response to "forge hub" community](https://matrix.to/#/!SakSkZqjzMsaPCVqlv:matrix.batsense.net/$NW4cKP1-k3yFR_ByIJQTVL-iVc7tCGm08YtS8W9hbUs?via=matrix.org&via=t2bot.io&via=matrix.batsense.net) dedicated to federating code forges such as Gitea, Gabe the Owncast maintainers told about ActivityPub implementation experience (highlights mine):

> I'm not sure I have any specific insights. **I think everyone does ActivityPub implementation differently**. For me I decided to use go-fed for modelling and serialization, but not at all for persistence, delivery, endpoints, etc. That way I could stay flexible and learn as I went along about what needs to be supported where, while at the same time using the go-fed vocabulary as guide rails when building and parsing ActivityPub payloads. I feel like it was a good compromise.
> 
> That also gave the benefit of storing full ActivityPub objects when I felt that was required, or just the IRI or slimmed down internal representations of these objects when it wasn't.
> 
> If I had to call out any particular challenge it's the fact that anything that speaks ActivityPub, with enough work, turns into a messaging application, and that was something I really didn't want with Owncast. If you send a message, and somebody replies, and then you want to reply to that, and then you have threads, etc etc eventually you're just rebuilding Mastodon on top of what you really meant to build.

#### Owncast ← Peertube observations

Looking at the usefulness of Peertube as a guide for Owncast to add federation support to the codebase.

- Implemented in Golang not Typescript, using [Go-Fed](https://github.com/go-fed) and different requirements, Peertube likely had limited guidance to offer.
- In the AP implementation issue [Cory Slep of Go-Fed comments](https://github.com/owncast/owncast/issues/458#issuecomment-753505317) and cross-references to his [design issue](https://github.com/go-gitea/gitea/issues/14186) in Gitea issue tracker.
  - An example of Ad-hoc Interoperability, in this case in the Go-Fed ecosystem, where the website docs weren't sufficient.
- Owncast has an early open issue on a P2P HLS implementation, where [someone commented](https://github.com/owncast/owncast/issues/112#issuecomment-764409536) Peertube HLS support.
  - A missed opportunity for Peertube and Owncast to collaborate, and create something better than exists now (see above).

#### Owncast → Peertube observations

Now that Owncast has federation support (see [ActivityPub project board](https://github.com/owncast/owncast/projects/2)) there's interest to federate with Peertube.

- Integration with Peertube was blocked from Aug to Oct '21 by [Peertube's incompatible HTTP Signature implementation](https://github.com/owncast/owncast/issues/1292).
  - Gabek [reported the issue at Peertube](https://github.com/Chocobozzz/PeerTube/issues/4431) and mentions root cause of NPM module [not supporting hs2019 algorithm](https://github.com/joyent/node-http-signature/issues/106).
  - Though now fixed [HTTP Signatures are not well-standardized on the Fediverse](https://socialhub.activitypub.rocks/t/state-of-http-signatures/754) still, or confusing as to the best-practices.
- [#1197 Research and implement PeerTube specific properties](https://github.com/owncast/owncast/issues/1197) is planned for [v0.0.12 Additional Federation features + IndieAuth](https://github.com/owncast/owncast/milestone/17).
  - Issue is immediately blocked by Peertube service actor expecting an `"url"` property to be present in a `Follow` request.
- If Owncast implements Peertube properties, should they add Peertube namespace to their JSON-LD context?
  - And AppY, AppZ namespaces for apps that come next? This is not a sane approach.
  - Common domain for vocab extensions makes sense à la `https://w3id.org`. Maybe `https://fedi.foundation/ns`?
  - Recently on SocialHub [A namespace for things defined in FEP's](https://socialhub.activitypub.rocks/t/a-namespace-for-things-defined-in-feps/2374) was started. But without involvement of people having the right Linked Data expertise.

#### Conclusion

- In this analysis we see typical example of Ad-hoc Interoperability at work. A better approach is not readily apparent.
- Each new app will introduce incompatibilities that make it harder for the next app to have broad interoperability.
- Chocobozz of Peertube acted to best of their ability. There was no substrate and little incentive to spend time creating one.

#### Additional resources

Peertube resources:

- [ActivityPub documentation](https://docs.joinpeertube.org/api-activitypub)
- [ActivityStreams JSON-LD](https://github.com/w3c/activitystreams/blob/master/ns/activitystreams.jsonld)
- [ActivityStreams OWL](https://github.com/w3c/activitystreams/blob/master/vocabulary/activitystreams2.owl)

#### Peertube ActivityPub vocabulary examples

Peertube JSON-LD context (used on all objects)

```json
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "RsaSignature2017": "https://w3id.org/security#RsaSignature2017"
    },
    {
      "pt": "https://joinpeertube.org/ns#",
      "sc": "http://schema.org#",
      "Hashtag": "as:Hashtag",
      "uuid": "sc:identifier",
      "category": "sc:category",
      "licence": "sc:license",
      "subtitleLanguage": "sc:subtitleLanguage",
      "sensitive": "as:sensitive",
      "language": "sc:inLanguage",
      "isLiveBroadcast": "sc:isLiveBroadcast",
      "liveSaveReplay": {
        "@type": "sc:Boolean",
        "@id": "pt:liveSaveReplay"
      },
      "permanentLive": {
        "@type": "sc:Boolean",
        "@id": "pt:permanentLive"
      },
      "Infohash": "pt:Infohash",
      "Playlist": "pt:Playlist",
      "PlaylistElement": "pt:PlaylistElement",
      "originallyPublishedAt": "sc:datePublished",
      "views": {
        "@type": "sc:Number",
        "@id": "pt:views"
      },
      "state": {
        "@type": "sc:Number",
        "@id": "pt:state"
      },
      "size": {
        "@type": "sc:Number",
        "@id": "pt:size"
      },
      "fps": {
        "@type": "sc:Number",
        "@id": "pt:fps"
      },
      "startTimestamp": {
        "@type": "sc:Number",
        "@id": "pt:startTimestamp"
      },
      "stopTimestamp": {
        "@type": "sc:Number",
        "@id": "pt:stopTimestamp"
      },
      "position": {
        "@type": "sc:Number",
        "@id": "pt:position"
      },
      "commentsEnabled": {
        "@type": "sc:Boolean",
        "@id": "pt:commentsEnabled"
      },
      "downloadEnabled": {
        "@type": "sc:Boolean",
        "@id": "pt:downloadEnabled"
      },
      "waitTranscoding": {
        "@type": "sc:Boolean",
        "@id": "pt:waitTranscoding"
      },
      "support": {
        "@type": "sc:Text",
        "@id": "pt:support"
      },
      "likes": {
        "@id": "as:likes",
        "@type": "@id"
      },
      "dislikes": {
        "@id": "as:dislikes",
        "@type": "@id"
      },
      "playlists": {
        "@id": "pt:playlists",
        "@type": "@id"
      },
      "shares": {
        "@id": "as:shares",
        "@type": "@id"
      },
      "comments": {
        "@id": "as:comments",
        "@type": "@id"
      }
    }
  ]

```

(Group) Peertube Channel of 'ActivityPub Conference on conf.tube'

```sh
curl -H 'Accept: application/ld+json' https://conf.tube/c/apconf_channel/videos
```

```json
{
  "type": "Group",
  "id": "https://conf.tube/video-channels/apconf_channel",
  "following": "https://conf.tube/video-channels/apconf_channel/following",
  "followers": "https://conf.tube/video-channels/apconf_channel/followers",
  "playlists": "https://conf.tube/video-channels/apconf_channel/playlists",
  "inbox": "https://conf.tube/video-channels/apconf_channel/inbox",
  "outbox": "https://conf.tube/video-channels/apconf_channel/outbox",
  "preferredUsername": "apconf_channel",
  "url": "https://conf.tube/video-channels/apconf_channel",
  "name": "ActivityPub Conference",
  "endpoints": {
    "sharedInbox": "https://conf.tube/inbox"
  },
  "publicKey": {
    "id": "https://conf.tube/video-channels/apconf_channel#main-key",
    "owner": "https://conf.tube/video-channels/apconf_channel",
    "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx6kJrWfpeWo6sCC1pCv2\njiwG9FqMUVRav/3La3eAf+u3LJ7xo3Dt7BH8ehdRmPbtXEoFN9fj8oqCuYyfzEnO\n4szk8dkiRkd69jzflF6c+DUUP/GzI3ZgrPOD3W+JV1MTLF42Ljz6yw24m/j45KZ2\n30DwWfOECT0v77ND5xNhXfT7DnyLnD1AacA89NSXaWOR27FN+H6sIVl0j99RzgG9\ndndKdMddWbHmIzU9KsMhCgHohYl6Id3HGFeGl222ZrK8p7yrbDvdpQ+ByoMSyp80\nNS18n70RcDpnj/nGrfwmx0n39L/3KXarovkZAoJFGuLvZwZ3ZaNa6j0FonIaxSVJ\nowIDAQAB\n-----END PUBLIC KEY-----"
  },
  "published": "2019-08-13T20:59:47.831Z",
  "icon": {
    "type": "Image",
    "mediaType": "image/png",
    "height": null,
    "width": null,
    "url": "https://conf.tube/lazy-static/avatars/fabd8922-c6b8-4cf0-a8ee-12f7f5d781d4.png"
  },
  "summary": "A conference about the present and future of ActivityPub, the world’s leading federated social web standard. This two day event will include presentations of prepared talks on Saturday followed by a loosely structured unconference on Sunday.",
  "support": null,
  "attributedTo": [
    {
      "type": "Person",
      "id": "https://conf.tube/accounts/apconf"
    }
  ]
}
```

(OrderedCollection) Peertube Playlists collection of 'ActivityPub Conference channel on conf.tube'

```sh
curl -H 'Accept: application/ld+json' https://conf.tube/video-channels/apconf_channel/playlists
```

```json
{
  "id": "https://conf.tube/video-channels/apconf_channel/playlists",
  "type": "OrderedCollectionPage",
  "totalItems": 1,
  "first": "https://conf.tube/video-channels/apconf_channel/playlists?page=1"
}
```

(PlayList) Peertube Playlist object in 'ActivityPub Conference channel on conf.tube'

```sh
curl -H 'Accept: application/ld+json' https://conf.tube/w/p/fwYkW8Yv1PDPHDCEtpCDoT
```

```json
{
  "id": "https://conf.tube/video-playlists/75b2fa34-a88a-4c89-969e-b63751c1d7d3",
  "type": "Playlist",
  "totalItems": 20,
  "first": "https://conf.tube/video-playlists/75b2fa34-a88a-4c89-969e-b63751c1d7d3?page=1",
  "name": "ActivityPub Conf 2020",
  "content": "A conference about the present and future of ActivityPub, the world’s leading federated social web standard.\r\n",
  "mediaType": "text/markdown",
  "uuid": "75b2fa34-a88a-4c89-969e-b63751c1d7d3",
  "published": "2020-09-26T19:09:48.863Z",
  "updated": "2020-09-29T12:42:11.166Z",
  "attributedTo": [
    "https://conf.tube/video-channels/apconf_channel"
  ],
  "icon": {
    "type": "Image",
    "url": "https://conf.tube/static/thumbnails/playlist-75b2fa34-a88a-4c89-969e-b63751c1d7d3.jpg",
    "mediaType": "image/jpeg",
    "width": 280,
    "height": 157
  },
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "https://conf.tube/accounts/apconf/followers"
  ]
}
```

(Video) Peertube Video part of Playlist in 'ActivityPub Conference channel on conf.tube'

```sh
curl -H 'Accept: application/ld+json' https://conf.tube/w/daJwZp7HhQFK5v3B9L89oY
```

```json
{
  "type": "Video",
  "id": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec",
  "name": "ActivityPub Conference 2020 opening keynote: The ActivityPub Panel",
  "duration": "PT3354S",
  "uuid": "6289920f-4e35-4141-ab6f-379b357849ec",
  "tag": [
    {
      "type": "Hashtag",
      "name": "activitypub"
    },
    {
      "type": "Hashtag",
      "name": "apconf"
    }
  ],
  "category": {
    "identifier": "15",
    "name": "Science & Technology"
  },
  "licence": {
    "identifier": "1",
    "name": "Attribution"
  },
  "language": {
    "identifier": "en",
    "name": "English"
  },
  "views": 287,
  "sensitive": false,
  "waitTranscoding": true,
  "isLiveBroadcast": false,
  "liveSaveReplay": null,
  "permanentLive": null,
  "state": 1,
  "commentsEnabled": true,
  "downloadEnabled": true,
  "published": "2020-09-20T12:50:01.869Z",
  "originallyPublishedAt": "2020-09-26T20:01:00.000Z",
  "updated": "2022-02-25T23:22:11.130Z",
  "mediaType": "text/markdown",
  "content": "including Jessica Tallon, Amy Guy, Evan Prodromou, and Erin Shepherd\r\nmoderated by Christopher Lemmer-Webber\r\n\r\nActivityPub is now a widely adopted standard... but how did it become a standard in the first place?\r\n\r\nHear about the process of getting ActivityPub all the way to W3C Recommendation status from the people who made it happen, as well as the history that lead to the decision to try and make ActivityPub a standard in the first place! This will be a panel of editors and authors of the ActivityPub protocol.",
  "support": null,
  "subtitleLanguage": [],
  "icon": [
    {
      "type": "Image",
      "url": "https://conf.tube/static/thumbnails/6289920f-4e35-4141-ab6f-379b357849ec.jpg",
      "mediaType": "image/jpeg",
      "width": 223,
      "height": 122
    },
    {
      "type": "Image",
      "url": "https://conf.tube/lazy-static/previews/6289920f-4e35-4141-ab6f-379b357849ec.jpg",
      "mediaType": "image/jpeg",
      "width": 850,
      "height": 480
    }
  ],
  "url": [
    {
      "type": "Link",
      "mediaType": "text/html",
      "href": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec"
    },
    {
      "type": "Link",
      "mediaType": "video/mp4",
      "href": "https://sb-conf-tube.b-cdn.net/videos/6289920f-4e35-4141-ab6f-379b357849ec-720.mp4",
      "height": 720,
      "size": 2172022819,
      "fps": 30
    },
    {
      "type": "Link",
      "rel": [
        "metadata",
        "video/mp4"
      ],
      "mediaType": "application/json",
      "href": "https://conf.tube/api/v1/videos/6289920f-4e35-4141-ab6f-379b357849ec/metadata/7020",
      "height": 720,
      "fps": 30
    },
    {
      "type": "Link",
      "mediaType": "application/x-bittorrent",
      "href": "https://conf.tube/lazy-static/torrents/90ed608a-bed6-4f00-9f15-778128dd7b8c-720.torrent",
      "height": 720
    },
    {
      "type": "Link",
      "mediaType": "application/x-bittorrent;x-scheme-handler/magnet",
      "href": "magnet:?xs=https%3A%2F%2Fconf.tube%2Flazy-static%2Ftorrents%2F90ed608a-bed6-4f00-9f15-778128dd7b8c-720.torrent&xt=urn:btih:daad19414e93b77ca46e9584a880b5decd50de6b&dn=ActivityPub+Conference+2020+opening+keynote%3A+The+ActivityPub+Panel&tr=https%3A%2F%2Fconf.tube%2Ftracker%2Fannounce&tr=wss%3A%2F%2Fconf.tube%3A443%2Ftracker%2Fsocket&ws=https%3A%2F%2Fsb-conf-tube.b-cdn.net%2Fvideos%2F6289920f-4e35-4141-ab6f-379b357849ec-720.mp4",
      "height": 720
    },
    {
      "type": "Link",
      "mediaType": "video/mp4",
      "href": "https://sb-conf-tube.b-cdn.net/videos/6289920f-4e35-4141-ab6f-379b357849ec-480.mp4",
      "height": 480,
      "size": 195470053,
      "fps": 30
    },
    {
      "type": "Link",
      "rel": [
        "metadata",
        "video/mp4"
      ],
      "mediaType": "application/json",
      "href": "https://conf.tube/api/v1/videos/6289920f-4e35-4141-ab6f-379b357849ec/metadata/7021",
      "height": 480,
      "fps": 30
    },
    {
      "type": "Link",
      "mediaType": "application/x-bittorrent",
      "href": "https://conf.tube/lazy-static/torrents/295f07ea-785b-4785-b89e-f89133e599e2-480.torrent",
      "height": 480
    },
    {
      "type": "Link",
      "mediaType": "application/x-bittorrent;x-scheme-handler/magnet",
      "href": "magnet:?xs=https%3A%2F%2Fconf.tube%2Flazy-static%2Ftorrents%2F295f07ea-785b-4785-b89e-f89133e599e2-480.torrent&xt=urn:btih:7a0d82f06ed8eedfeea0d65a8b6dbfb0192ccf07&dn=ActivityPub+Conference+2020+opening+keynote%3A+The+ActivityPub+Panel&tr=https%3A%2F%2Fconf.tube%2Ftracker%2Fannounce&tr=wss%3A%2F%2Fconf.tube%3A443%2Ftracker%2Fsocket&ws=https%3A%2F%2Fsb-conf-tube.b-cdn.net%2Fvideos%2F6289920f-4e35-4141-ab6f-379b357849ec-480.mp4",
      "height": 480
    },
    {
      "type": "Link",
      "name": "tracker-http",
      "rel": [
        "tracker",
        "http"
      ],
      "href": "https://conf.tube/tracker/announce"
    },
    {
      "type": "Link",
      "name": "tracker-websocket",
      "rel": [
        "tracker",
        "websocket"
      ],
      "href": "wss://conf.tube:443/tracker/socket"
    }
  ],
  "likes": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/likes",
  "dislikes": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/dislikes",
  "shares": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/announces",
  "comments": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/comments",
  "attributedTo": [
    {
      "type": "Person",
      "id": "https://conf.tube/accounts/apconf"
    },
    {
      "type": "Group",
      "id": "https://conf.tube/video-channels/apconf_channel"
    }
  ],
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "https://conf.tube/accounts/apconf/followers"
  ]
}
```

(Note) Peertube Comment on Video

```json
{
  "type": "Note",
  "id": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/comments/949",
  "content": "**[Q&A Session – Opening Keynote: The ActivityPub Panel](https://conf.tube/videos/watch/2cabb25c-0979-4ef6-beaf-f3d001ecbc4c)<br />⬡ Hooray, the live Questions & Answers are available here<br />https://conf.tube/videos/watch/2cabb25c-0979-4ef6-beaf-f3d001ecbc4c",
  "mediaType": "text/markdown",
  "inReplyTo": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec",
  "updated": "2020-10-19T18:46:00.072Z",
  "published": "2020-10-19T18:46:00.048Z",
  "url": "https://conf.tube/videos/watch/6289920f-4e35-4141-ab6f-379b357849ec/comments/949",
  "attributedTo": "https://conf.tube/accounts/apconf",
  "tag": [],
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "https://conf.tube/accounts/apconf/followers"
  ]
}
```

(VideoObject) Schema.org `meta` snippet generated on Video pages

```json
{
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "ActivityPub Conference 2020 opening keynote: The ActivityPub Panel",
  "description": "including Jessica Tallon, Amy Guy, Evan Prodromou, and Erin Shepherdmoderated by Christopher Lemmer-Webber ActivityPub is now a widely adopted standard... but how did it become a standard in the first place? Hear about the process of getting ActivityPub all the way to W3C Recommendation status from the people who made it happen, as well as the history that lead to the decision to try and make ActivityPub a standard in the first place! This will be a panel of editors and authors of the ActivityPub protocol.",
  "image": "https://conf.tube/lazy-static/previews/6289920f-4e35-4141-ab6f-379b357849ec.jpg",
  "url": "https://conf.tube/w/daJwZp7HhQFK5v3B9L89oY",
  "embedUrl": "https://conf.tube/videos/embed/6289920f-4e35-4141-ab6f-379b357849ec",
  "uploadDate": "2020-09-20T01:58:44.000Z",
  "duration": "PT3354S",
  "iterationCount": 288,
  "thumbnailUrl": "https://conf.tube/lazy-static/previews/6289920f-4e35-4141-ab6f-379b357849ec.jpg",
  "contentUrl": "https://conf.tube/w/daJwZp7HhQFK5v3B9L89oY"
}
```

### Example 3: Post-facto interoperability

{: .highlight}
> **Leading implementation → New entrant**. This example is about how popular applications can dominate the ecosystem and force other projects to follow their lead.

This is a possible case of Post-facto interop, [newly encountered](https://framapiaf.org/@Liwott/108228687561368244) issues with federation between Mastodon and Lemmy. The federation with Mastodon was working fine when Lemmy first introduced federation support to other applications than Lemmy.

#### Lemmy federation to Mastodon breaks down

- In Friendica you can create new Lemmy posts, but Mastodon until now never supported that.
- When Lemmy launched federation support, on Mastodon you could properly interact with Lemmy posts of followed Communities.
- Federation functionality to Mastodon is now broken due to (likely) changes on Mastodon side. There seem to be multiple issues.
- One bug mentioned [in reply](https://some.nrsk.no/@userthree/108228792967916535) to the discussion sees Mastodon boosting all Lemmy comments, which is spamming timelines.
- [@userthree](https://some.nrsk.no/@userthree) creates a Lemmy issue: [Federating with Mastodon - Don't boost comments](https://github.com/LemmyNet/lemmy/issues/2224)
  - An analysis of core devs of Lemmy (Nutomic) and Mastodon (ClearlyClaire) ensues.
  - Lemmy announces group actions by wrapping in `Announce` e.g. `Announce{Create{Note}}`.
  - This was unsupported on Mastodon so Lemmy created a fallback to send `Announce{Note}`.
  - Earlier issue of 'too much noise' led to [Dont announce comments, edited posts to Pleroma/Mastodon followers](https://github.com/LemmyNet/lemmy/pull/1914)
  - So Lemmy changed it to `Announce{Page}` and `Announce{Create{Page}}`. On Mastodon this breaks Post comment threads.
  - It was observed that both Lemmy and Mastodon have (their own different) shortcomings to reply-handling.
  - Boosted comments bug was found to be caused by new functionality added in Mastodon 3.5 and an unintended side-effect.
  - Mastodon PR causing bug is [Add support for fetching Create and Announce activities by URI](https://github.com/mastodon/mastodon/pull/16383) in prep for Post Editing, Group support.
  - Nutomic now closes the issue, as not Lemmy-related.
- [@userthree](https://some.nrsk.no/@userthree) now creates [a similar issue](https://github.com/mastodon/mastodon/issues/18069) in the Mastodon issue tracker, pointing to the closed Lemmy issue analysis.
  - [@Nightpool](https://github.com/mastodon/mastodon/issues/18069#issuecomment-1107160599) of Mastodon reacts that 'this makes no sense', everything works as it should, closes the issue.
  - This comment came affter Nutomic already closed the Lemmy issue, so they likely are not aware of it (I added a heads-up).
- In the fediverse discussion [@Liwott mentions](https://framapiaf.org/@Liwott/108228975566091808) that closing both issues like that is 'a bit harsh', and there's benefit to fixing on both sides.
  - Note: The benefit is clearly greater on Lemmy's side, the smaller party, who will miss out on a lot of Mastodon-based interaction.

Now, it is not 100% clear if this is post-facto interop, where Mastodon on their own as dominant part introduces new features (Post Editing) that breaks other application's functionality unless they catch up. This may also relate to the ongoing ad-hoc interop where everyone adds their own Groups functionality. See [Ad-hoc interoperability: Groups standardization](https://notes.smallcircles.work/cp38nGmoRRG_i3LS_S1l8w#).

#### Observations

- There are no standards to this. Every app is iterating on their own, creating possible breakages with other apps.
- There is no visibility to what is happening. Knowledge is spread in multiple issue trackers, many issues, numerous cross-references.
- Only involved expert devs have a clear picture what is going on, at the moment of analysis. They'll likely forget much of that context.
- Numerous people report/discuss the observed bugs, and eat time of the devs having to respond  (see e.g. [this Lemmy post](https://lemmy.ml/post/250654)).
- Github helps by creating cross-references between issues, but for apps on different code forges this benefit doesn't exist.

#### Conclusions

- Dominant, most active and fast-moving project will dictate the protocol implementation and changes, other projects must follow.
- Breakages like this will become the norm, and fixing them a 'whack-a-mole' time-absorbing and frustrating process.
- Likely many apps won't spend effort in broad federation, federate just with themself or their domain (fractured interop islands).
- Likely all the breakages will turn off fedizens, who'll either stick to dominant/popular apps, or leave the Fediverse altogether.

→ **Post-facto interoperability pays off, and leading positions of already dominant apps will be further cemented.**