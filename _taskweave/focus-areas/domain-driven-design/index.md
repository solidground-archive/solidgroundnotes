---
layout: default
title: Domain Driven Design
parent: Key focus areas
has_children: true
nav_order: 3
permalink: process/focus/domain-driven-design
---

# Domain Driven Design

## About domain driven design

A Domain is a specific field of knowledge or activity, and domain driven design (DDD) is a structured approach to analyse and elaborate this field in a way that makes it easier to translate into the appropriate source code abstractions of a software application. DDD consists of two separate parts: It starts with Strategic design and is optionally implemented using Tactical patterns.

{: .note }
> Software projects _always_ engage in domain analysis, but this stage is often not recognized, or at best an informal process and certainly not strategic.

Taskweave focuses primarily on strategic domain driven design. The process is intuitive, doesn't add much overhead, and can be combined with existing agile development methods a software team prefers to follow.

{: .highlight }
> **TODO**: Briefly summarize the advantages of applying strategic design.

## Elements of strategic design

Domain driven strategic design involves the gradual elaboration with the domain experts and the technical team of three main artifacts:

1. **Ubiquitous language**: Common terminology that is used and well-understood by all stakeholders.
2. **Bounded contexts**: Breakdown model of the domain into internally consistent sub-domains.
3. **Context map**: Relationships between bounded contexts and external systems.

These three artifacts will continue to evolve throughout the project. They are part of the living documentation.

### Ubiquitous language

The ubiquitous language is the terminology that everyone involved in a software project agrees to use, and it must represent the domain that is being modeled.

[![Diagram that shows the central role of the ubiquitous language](/assets/images/ddd-ubiquitous-language.gif)](https://www.infoq.com/articles/ddd-contextmapping/)

<small><i>Figure: Everyone in a project agrees to follow the ubiquitous language.</i></small>

Domain experts are leading in discovering this language. They will be using the software after all. But this terminology will then be used throughout the project and find its way into the code to name variables and run tests. An agreed upon unambiguous language will increase common understanding, and lead to software that better aligns with the needs of the stakeholders.

### Bounded contexts

During domain driven analysis a top-level domain is decomposed into multiple sub-domains. We call these bounded context, and they define the consistency boundaries for the domain concept and relationships defined within.

[![Diagram that shows a context map for Personal Banking domain](/assets/images/ddd-context-map.jpg)](https://www.infoq.com/articles/ddd-contextmapping/)

<small><i>Figure: A Personal Banking domain consisting of several bounded contexts.</i></small>

Some bounded contexts are more relevant than others. There are three different domain types:

1. **Core domains**: Contain the key functionality of the social experience.
2. **Supportive domains**: Less important than the core domain, but closely related.
3. **Generic domains**: Functionality that is common, not instrumental to project success.

#### Bounded context informs architecture design

A Bounded context that is sufficiently elaborated informs the architecture decision for its implementation. For instance a "User Management" generic domain may be implemented by an off-the-shelf external SaaS service.

Floorplanner will support multiple architecture patterns to choose from:

1. **Service Wrapper**: A library, framework or external service is adopted that implements (most of) the bounded context. Generic domains are candidates to be implemented this way.
2. **CRUD**: If the bounded context consists mostly of Create, Read, Update, Delete of domain concepts, then this straightforward pattern is selected.
3. **CQRS**: More elaborate context may benefit from separating Read-side from the Write-side. The architecture supports Command and Query operations.
4. **CQRS/ES**: CQRS extended with Event Sourcing. The most flexible but also complex architecture pattern allows implementation of complex domain models.

{: .todo}
> Document Architecture Patterns separately e.g. in a Process recipe.

All the architecture patterns support the event driven architecture of Groundwork. Domain events are how bounded contexts communicate with each other, and federate across the internet.

In many cases bounded contexts will eventually be implemented one-to-one as sub-system components of the architecture. For Groundwork they are then likely packaged into separate Service Modules. But this is not a rule. A Service Module can implement multiple bounded contexts.

## More information

| [**Domain Driven Design Reference**](https://www.domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf): (PDF) Definitions and Pattern Summaries by Erik Evans. |
| [**Domain-driven Design Quickly**](https://www.infoq.com/minibooks/domain-driven-design-quickly/): InfoQ Mini-book that provides a short, quick-readable summary and introduction to the fundamentals of DDD. |
| [**DDD Strategic Design in under 15 minutes**](https://yewtu.be/watch?v=Evers5npkmE): Video by Alpha Code that provides a quick overview of the most important Strategic Design tools. |