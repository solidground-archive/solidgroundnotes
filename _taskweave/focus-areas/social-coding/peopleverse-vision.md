---
layout: default
title: Peopleverse Vision
parent: Social Coding
nav_order: 1
permalink: process/focus/social-coding/peopleverse
---

# Towards the Peopleverse
{: .fs-9 .no_toc }

How Solidground dedicates to a shared vision for the future of the Fediverse.
{: .fs-6 .fw-500 }

<br>

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .challenge}
> Here we evaluate how Solidground can contribute to tackling the major challenge for the Fediverse of the [Lack of a Shared (Technology) Vision](https://discuss.coding.social/t/challenge-adopt-a-shared-technology-vision-for-the-fediverse/45).

## Shared Vision

Having a shared vision early on in a project and revisiting it frequently to check if it still matches the goals and objectives, is very important to keep a project on track. Our [Shared Vision](/process/inception-phase/shared-vision) process recipe provides guidance for your social experience design.

However, the shared vision doesn't stop there. Practicing SX entails creating [integrated experience](/process/focus/integrated-experience), and requires consideration how your solution fits in with other social experiences on the Fediverse. Subsequent alignment with other projects requires collaboration and collective action.

## The Peopleverse

