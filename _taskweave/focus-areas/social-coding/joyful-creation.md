---
layout: default
title: Joyful Creation
parent: Social Coding
has_children: true
nav_order: 3
permalink: process/focus/social-coding/joyful-creation
---

# Joyful Creation
{: .fs-9 .no_toc }

Take away the chores of development, leaving more time for the fun of creating solutions that matter.
{: .fs-6 .fw-500 }

<br>

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .challenge}
> Here we address the major challenge for the Fediverse of the [Broken Technology Adoption Lifecycle](https://discuss.coding.social/t/challenge-fixing-the-fediverse-technology-adoption-lifecycle/38).

## CUPID Joyful Coding