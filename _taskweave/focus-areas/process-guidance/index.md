---
layout: default
title: Process Guidance
parent: Key focus areas
has_children: true
nav_order: 2
permalink: process/focus/process-guidance
---

# Process Guidance

## Living Specifications

## Living Documentation

On this page ideas and requirements for documentation are elaborated that aim to:

- Ease the burden of doing documentation chores, so it is more likely to be maintained.
- Ways of keeping documentation up-to-date and of high quality and consistency.
