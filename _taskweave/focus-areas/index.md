---
layout: default
title: Key focus areas
has_children: true
nav_order: 2
permalink: process/focus
---

# Exploring SX
{: .fs-9 .no_toc }

How to transition from app development to Social Experience Design of integrated Fediverse solutions.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .note}
> This page contains summary of findings for Taskweave, derived from broad exploration. Details can be found in the child pages.

## Focus areas

What makes federated application development particularly challenging, and how can we address these challenges with Taskweave and Groundwork? Exploration found a number of focus areas:

| Focus area | Description |
| :--- | :--- |
| [**Ad-hoc interoperability**](./adhoc-interop) | App-specific federation extensions that are created on-the-fly and can only interoperate on the basis of app-by-app integrations. |
| [**Substrate formation**](./substrate-formation) | People, processes, tools and artifacts that collaboratively ease onboarding and adoption of interoperable mechanism, that allow the ecosystem as a whole to evolve. |
| Free software development lifecycle | Typical motivations, expectations, development processes and practices that drive successful free software projects. |

## Major challenges

While exploring the focus areas, several major challenges have been found. These challenges relate to the Fediverse as a whole. The Solidground project wants to address them where possible, so as to add most value to those using the process.

| | Challenge | Description |
| :---: | :--- | :--- |
| CHA-1 | [**Lack of a shared (technology) vision**](https://discuss.coding.social/t/challenge-adopt-a-shared-technology-vision-for-the-fediverse/45) | People just use the Fediverse in its current state without asking what it could be in the future. The potential and opportunities are insufficiently explored. |
| CHA-2 | [**Fix Fediverse Adoption Lifecycle**](https://discuss.coding.social/t/challenge-fixing-the-fediverse-technology-adoption-lifecycle/38) | A combination of factors make onboarding and "crossing the chasm" very hard, and combined with growing complexity in the ecosystem this stifles innovation. |
| CHA-3 | [**Substrate Formation for the Fediverse**](https://discuss.coding.social/t/challenge-healthy-substrate-formation-for-the-fediverse/63) | The technological landscape is fragmented and opaque, community is dispersed and not very collaborative, while the processes for evolving the ecosystem are immature or undefined. |
| CHA-4 | [**Representation of the Fediverse**](https://discuss.coding.social/t/challenge-representation-of-the-fediverse/65) | With nobody able to represent the Fediverse, it is not possible to anticipate to developments elsewhere, reach out and collaborate with other communities, and follow collective strategies for Fediverse evolution. |

## Solutions

### Become part of Social Coding Movement (CHA-3)

Solidground will not only be a practitioner of [Social Coding](https://coding.social), but be part of the co-shared [Discuss Social Coding](https://discuss.coding.social) community forum. Solidground will:

- **Adopt** best-practices and implement features that tackle social coding challenges.
- **Contribute** documentation to social coding website relating to challenges and best-practices.
- **Participate** to help reach substrate formation objectives of Social Coding Foundations.

### Encourage Living Documentation best-practices (CHA-2)

A structured process is ideal for creating consistent documentation. Solidground strives to lower barriers and maintain living documentation, to the benefit of both the project and ecosystem.

- **Templates, diagrams, checklists** to guide stakeholders along, lessen documentation chores.
- **Code generation** from templates, diagrams, checklists, encourage documentation consistency.

### Taskweave contributes to Fediverse substrate (CHA-2)

Fediverse has near non-existent substrate to its ecosystem. [Fediverse Enhancement Proposal](https://codeberg.org/fediverse/fep) (FEP) process is immature, and [Socialhub community](https://socialhub.activitypub.rocks) unable to foster ecosystem evolution.

Solidground will focus on formulating procedures and find best-practices that benefit substrate formation. The project strives to contribute to the Fediverse ecosystem at three levels:

1. **Open standards**: Help continue the work that started in the W3C so that new standards may emerge. This may involve setting up a new governance structure.
2. **Protocol extensions**: Help mature the FEP process so it becomes relevant and useful, and brings clarity and reduced complexity in creating interoperable applications.
3. **Vocabulary extensions**: Help provide comprehensive guidance and organization on how both application- and domain-specific ActivityPub extensions are to be modeled.