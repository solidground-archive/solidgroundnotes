---
layout: default
title: Host your experience
nav_order: 1
permalink: groundwork/platform
---

# Host on Groundwork

{: .info }
> Groundwork has just started, and functionality described here is subject to change.

## Overview

The Groundwork service platform makes hosting of social experiences as easy as possible. It does the heavy lifting so that system administrators can focus on offering quality services. A social experience when deployed on the server consist of one or more [Service Modules](/groundwork/project/design#service-modules) that can be managed separately.

Service modules represent [_bounded contexts_](/process/focus/domain-driven-design#bounded-contexts) of your domain and may interact with other modules according to the design of your [Floorplanner Blueprint](/floorplanner/design). Service modules are extensions for the Fediverse and can be invoked by other federated applications that want to integrate with your social experience. As the ecosystem around Solidground project grows a library of reusable Groundwork services modules will emerge over time.

So Groundwork is a **federated service platform** and from a conceptual viewpoint looks like this:

<div align="center">

<img src="{{ 'assets/images/groundwork-service-platform-concept_optimized.svg' | absolute_url }}" alt="Groundwork conceptual overview">

</div>

_"Present meaningful information"_ is Groundwork's purpose. With Groundwork you deploy and host your own business domain packaged as a social experience for the Fediverse.

Groundwork is Free Software. The self-hostable federated service platform is optimized for small deployments, but it is able to scale. Groundwork is easy to install and can be configured from a command-line installation script.