---
layout: default
title: Blog
nav_order: 4
permalink: blog
---

# We blog at Fedi Foundation
{: .fs-9 .no_toc }

Where we introduce Social Experience Design to our passionate fedizens.
{: .fs-6 .fw-500 }

---

## 2022

{: .todo}
> Transfer a whole bunch of documents from various locations to fedi foundation articles.

| 2022-09-28 &nbsp; [Let's Reimagine Social](https://fedi.foundation/2022/09/social-networking-reimagined/) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg>  |
| :--- |
| People are social creatures, and social networking is part of human nature. Social Media have clouded our vision and distorted what 'social' means. The Social Coding Movement aims to Reimagine Social Networking and envision a Peopleverse. |
