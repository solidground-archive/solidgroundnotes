---
layout: default
title: Community
nav_order: 2
permalink: community
---

# Join Us In Our Quest
{: .fs-9 .no_toc }

Become part of our community of Social Experience Designer.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Our community

We practice [Social Coding](https://coding.social/principles). Exploring the Free Software Development Lifecycle (FSDL), we seek broad participation  and provide help for projects and ecosystems to succeed. We see the Fediverse as the future for social networking, where a Peopleverse is on the rise. We are passionate to see it evolve. We continually improve our practices, to increase the support we can provide. 

## Project organization

These tech docs focus on Roadmap, Requirements, Architecture, and Implementation. Our [source code](https://codeberg.org/solidground) lives on Codeberg. We chat on [Matrix](https://matrix.to/#/#solidground-matters:matrix.org) and discuss on our [Discourse](https://discuss.coding.social/c/solidground/13) forum.

In parallel to our core services we develop two real-world [Showcases](/#learn-from-our-showcases).

## Join the team

We are looking for Designers and Coders and any motivated person that wants to contribute to our efforts. Do you join our group? Besides being part of a cool free software project, we help each other to benefit the most from our participation. [Get grounded in our community.](https://solidground.work/#project)

### Designers

Can you help with UX design, elaborate requirements and architecture concepts? We have exciting challenges to figure out. We value humane technology, privacy, security, and ethics in all our design. Join the fun and hone your skills.

### Coders

Do you want to code on backend server code, libraries or frontend UI's? Learn how domain-driven design (DDD) can ease development taks and implement event-driven architecture for the Fediverse. Don't hesitate. Come code with us.

## Code of Conduct

All our projects, code repositories, communication channels and the discussion forum are governed by the [Social Coding Community Participation Guidelines](https://codeberg.org/SocialCoding/community/src/branch/main/CODE_OF_CONDUCT.md). By your interaction you accept to abide by its rules.